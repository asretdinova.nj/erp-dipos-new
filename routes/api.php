<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('api')->get('/user', function (Request $request) {
    return \App\User::all();
});*/

//Route::post('login', 'API\UserController@login');
//Route::post('register', 'API\UserController@register');


Route::post('/login', 'API\AuthController@login');
Route::post('/register', 'API\AuthController@register');

Route::group(['middleware' => 'auth:api'], function(){
  Route::post('details', 'API\UserController@details');
});